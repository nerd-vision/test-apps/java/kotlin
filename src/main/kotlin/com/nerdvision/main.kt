/*
 *    Copyright 2021 Intergral GmbH / nerdvision / opensource
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


@file:Suppress("WarningOnMainUnusedParameterMigration")

package com.nerdvision

import com.nerdvision.api.NerdVisionAPI

fun main(args: Array<String>) {
    val ts = SimpleTest("This is a test")
    while (true) {
        try {
            ts.message(ts.newId())
        } catch (e: Exception) {
            NerdVisionAPI.captureException(e)
            e.printStackTrace()
            ts.reset()
        }
        Thread.sleep(100)
    }
}

/*
 *    Copyright 2021 Intergral GmbH / nerdvision / opensource
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


package com.nerdvision

import java.util.*
import java.util.concurrent.ThreadLocalRandom

open class BaseTest {
    protected val systemProps = System.getProperties()


    fun newId(): String {
        return UUID.randomUUID().toString()
    }


    protected fun nextMax(): Int {
        return ThreadLocalRandom.current().nextInt(1, 100)
    }


    fun makeCharCountMap(str: String): Map<Char, Int?> {
        val res = HashMap<Char, Int>()
        for (element in str) {
            val cnt = res[element]
            if (cnt == null) {
                res[element] = 0
            } else {
                res[element] = cnt + 1
            }
        }
        return res
    }
}

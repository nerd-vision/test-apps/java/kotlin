/*
 *    Copyright 2021 Intergral GmbH / nerdvision / opensource
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


package com.nerdvision

import java.util.*

class SimpleTest(val testName: String?) : BaseTest() {
    var NICE_START_DATE = Date()

    private val startedAt = System.currentTimeMillis()
    private var maxExecutions = nextMax()
    private var cnt = 0
    private var charCounter: MutableMap<Char, Int?> = TreeMap()

    @Throws(Exception::class)
    fun message(uuid: String) {
        println("$cnt:$uuid")
        cnt += 1
        checkEnd(cnt, maxExecutions)
        val info: Map<Char, Int?> = makeCharCountMap(uuid)
        merge(charCounter, info)
        if (cnt % 30 == 0) {
            dump()
        }
    }


    fun merge(charCounter: MutableMap<Char, Int?>, newInfo: Map<Char, Int?>) {
        for (c in newInfo.keys) {
            val i = newInfo[c]
            val curr = charCounter[c]
            if (curr == null) {
                charCounter[c] = i
            } else {
                charCounter[c] = curr + i!!
            }
        }
    }


    fun dump() {
        println(charCounter)
        charCounter = HashMap()
    }


    @Throws(Exception::class)
    fun checkEnd(`val`: Int, max: Int) {
        if (`val` > max) {
            throw Exception("Hit max executions $`val` $max")
        }
    }


    override fun toString(): String {
        return javaClass.name + ":" + testName + ":" + startedAt + "#" + System.identityHashCode(this)
    }


    fun reset() {
        cnt = 0
        maxExecutions = this.nextMax()
    }
}

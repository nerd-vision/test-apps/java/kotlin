#!/bin/bash
export NV_NAME=${NV_NAME:='kotlin-test-app'}
export NV_REPO_URL=${NV_REPO_URL:='https://gitlab.com/nerd-vision/test-apps/java/kotlin'}

curl -L "https://repository.sonatype.org/service/local/artifact/maven/redirect?r=central-proxy&g=com.nerdvision&a=agent&v=LATEST" --output "/opt/service/nerdvision.jar"
java -javaagent:/opt/service/nerdvision.jar -jar /opt/service/kotlin-test.jar
